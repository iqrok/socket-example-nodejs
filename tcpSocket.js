/*
In the node.js intro tutorial (http://nodejs.org/), they show a basic tcp
server, but for some reason omit a client connecting to it.  I added an
example at the bottom.
Save the following server in example.js:
*/

var net = require('net');
var date = require('date-and-time');

var server = net.createServer(function(socket) {
	socket.write('Echo server '+ date.format(new Date(), "YYYY-MM-DD HH:mm:ss") +'\r\n');
	socket.pipe(socket);
});

server.listen(1337, '192.168.88.253');

server.on('listening', function() {
	console.log('Echo server '+ date.format(new Date(), "YYYY-MM-DD HH:mm:ss") +'\r\n');
});

server.on('connection', function(socket) {
    console.log('A new connection has been established from '+ socket.remoteAddress);

    // Now that a TCP connection has been established, the server can send data to
    // the client by writing to its socket.
    socket.write('Hello, client.');

    // The server can also receive data from the client by reading from its socket.
    socket.on('data', function(chunk) {
		const data = chunk.toString();

		try{
			const obj = JSON.parse(data);
			console.log('============= JSON =====================');
			console.log(`Data [`+date.format(new Date(), "YYYY-MM-DD HH:mm:ss")+`] : `);
			console.dir(obj);
			console.log('============= JSON =====================');
		}
		catch(error){
			console.error('Not JSON :', error);
			console.log(`Data [`+date.format(new Date(), "YYYY-MM-DD HH:mm:ss")+`] : ${chunk.toString()}`);
		}
    });

    // When the client requests to end the TCP connection with the server, the server
    // ends the connection.
    socket.on('end', function() {
        console.log('Closing connection with the client'+ socket.remoteAddress);
        console.log('----------------------------------');
    });

    // Don't forget to catch error, for your own sake.
    socket.on('error', function(err) {
        console.log(`Error: ${err}`);
    });
});
/*
And connect with a tcp client from the command line using netcat, the *nix
utility for reading and writing across tcp/udp network connections.  I've only
used it for debugging myself.
$ netcat 127.0.0.1 1337
You should see:
> Echo server
*/
